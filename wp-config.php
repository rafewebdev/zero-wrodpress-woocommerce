<?php
/**
 * A configuração de base do WordPress
 *
 * Este ficheiro define os seguintes parâmetros: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, e ABSPATH. Pode obter mais informação
 * visitando {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} no Codex. As definições de MySQL são-lhe fornecidas pelo seu serviço de alojamento.
 *
 * Este ficheiro contém as seguintes configurações:
 *
 * * Configurações de  MySQL
 * * Chaves secretas
 * * Prefixo das tabelas da base de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Definições de MySQL - obtenha estes dados do seu serviço de alojamento** //
/** O nome da base de dados do WordPress */
define( 'DB_NAME', 'woocomerce_plugin' );

/** O nome do utilizador de MySQL */
define( 'DB_USER', 'root' );

/** A password do utilizador de MySQL  */
define( 'DB_PASSWORD', '' );

/** O nome do serviddor de  MySQL  */
define( 'DB_HOST', 'localhost' );

/** O "Database Charset" a usar na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O "Database Collate type". Se tem dúvidas não mude. */
define( 'DB_COLLATE', '' );

/**#@+
 * Chaves únicas de autenticação.
 *
 * Mude para frases únicas e diferentes!
 * Pode gerar frases automáticamente em {@link https://api.wordpress.org/secret-key/1.1/salt/ Serviço de chaves secretas de WordPress.org}
 * Pode mudar estes valores em qualquer altura para invalidar todos os cookies existentes o que terá como resultado obrigar todos os utilizadores a voltarem a fazer login
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY', '?.mxbTX& (|*+#iVvO)7;gsTi!_HM6]}_YS3$9|{}lekA>3%QYPtrrN~r)^Cs40@' );
define( 'SECURE_AUTH_KEY', '9O.bpZmIP}Ub+0* /5E@ILwxhU[_-ggkg@Ky<j~Yn*W8mGIH|IrrpVG{cuq:ph4<' );
define( 'LOGGED_IN_KEY', 'BphS[A?_Lm=as|BWWV}^DkT>p%i#Dpc)zv%ps3WPx~P* YwNE+7?kv#JZ@rrHXc+' );
define( 'NONCE_KEY', '`>,CQE*Sqa^ vH57Uvz>M6DlV2G6Ya#d`fD<aU7WE($ &N}ZfPGv<P_LsEtIU5A)' );
define( 'AUTH_SALT', ')ZFw8{_))t/xA9SHpV_]0Fi|t?TIv_M0@Pury`.<<;&C$&[w{ZnU#f$.2kIn+&q}' );
define( 'SECURE_AUTH_SALT', 'XiOZ4;(2;M|^r*#}l76J<Ue56@x^7 ->wAo[G*M7{pq({A?Qt:iIAEwxL2PvX@TF' );
define( 'LOGGED_IN_SALT', 's!zsz~S#1j9v)`t?mdH(IcJHRT/mtoz(6*S&UDy[j]YeQ!&jRq4D%AeV<}g^{0qU' );
define( 'NONCE_SALT', 'O]nnOuYCDZ4D.KDG$6^=&g_uLPAexJKen0A+BiGS!:`%l+p=Y8t^Cy8o*gxu >aV' );

/**#@-*/

/**
 * Prefixo das tabelas de WordPress.
 *
 * Pode suportar múltiplas instalações numa só base de dados, ao dar a cada
 * instalação um prefixo único. Só algarismos, letras e underscores, por favor!
 */
$table_prefix = 'wp_';

/**
 * Para developers: WordPress em modo debugging.
 *
 * Mude isto para true para mostrar avisos enquanto estiver a testar.
 * É vivamente recomendado aos autores de temas e plugins usarem WP_DEBUG
 * no seu ambiente de desenvolvimento.
 *
 * Para mais informações sobre outras constantes que pode usar para debugging,
 * visite o Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* E é tudo. Pare de editar! */

/** Caminho absoluto para a pasta do WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Define as variáveis do WordPress e ficheiros a incluir. */
require_once( ABSPATH . 'wp-settings.php' );
