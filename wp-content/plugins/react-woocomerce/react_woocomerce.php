<?php
/**
 * Plugin Name: Hello World React Plugin
 */

function react_woocomerce_shortcode() {
	return '<div id="react-woocomerce" ></div>';
}

add_shortcode('react_woocomerce', 'react_woocomerce_shortcode');

function react_woocomerce_load_assets() {
	$react_app_js  = plugin_dir_url( __FILE__ ) . 'reactwoocomerce/build/static/js/all_in_one_file.js';
    $react_app_css = plugin_dir_url( __FILE__ ) . 'reactwoocomerce/build/static/css/somecss.css';
    // time stops stylesheet/js caching while in development, might want to remove later
    $version = time();
    wp_enqueue_script( 'react-woocomerce', $react_app_js, array(), $version, true );
    wp_enqueue_style( 'react-woocomerce', $react_app_css, array(), $version );
}

add_action( 'wp_enqueue_scripts', 'react_woocomerce_load_assets' );